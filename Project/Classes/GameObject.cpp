#include "GameObject.h"

GameObject::GameObject(const std::string& filename,Type t)
{
	sprite = Sprite::create(filename);
	velocity_ = Vec2(0,0);
	type_ = t;
	active_ = true;
	size_ = Vec2(0, 0);
	grounded = false;
}

void GameObject::update() {

	//update velocity - useds for player
	sprite->getPhysicsBody()->setVelocity(Vec2(velocity_.x, sprite->getPhysicsBody()->getVelocity().y + velocity_.y));

}