#include "FuzzyInferenceSystem.h"

Difficulty::Difficulty() {
	//-----------------------hazards-------------------------
	//initialise membership functions for rule
	hazzards_[0] = new Trapezoid;
	hazzards_[1] = new Triangle;
	hazzards_[2] = new Trapezoid;

	//set values for decrese rule
	hazzards_[0]->setInterval(3, 10);
	hazzards_[0]->setMiddle(5, 10);
	hazzards_[0]->setType('r');
	hazzards_[0]->setName("decrease");
	
	//set values for maintain rule
	hazzards_[1]->setInterval(0, 6);
	hazzards_[1]->setMiddle(3, 3);
	hazzards_[1]->setType('t');
	hazzards_[1]->setName("maintain");

	//set values for increase rule
	hazzards_[2]->setInterval(-1, 3);
	hazzards_[2]->setMiddle(-1, 0);
	hazzards_[2]->setType('r');
	hazzards_[2]->setName("increase");


//-----------------------health-------------------------
	//initialise membership functions for rule
	health_[0] = new Trapezoid;
	health_[1] = new Triangle;
	health_[2] = new Trapezoid;

	//set values for decrease rule
	health_[0]->setInterval(-1, 50);
	health_[0]->setMiddle(25, 25);
	health_[0]->setType('r');
	health_[0]->setName("decrease");

	//set values for maintain rule
	health_[1]->setInterval(0, 100);
	health_[1]->setMiddle(50, 50);
	health_[1]->setType('t');
	health_[1]->setName("maintain");

	//set values for increase rule
	health_[2]->setInterval(50, 101);
	health_[2]->setMiddle(75, 101);
	health_[2]->setType('r');
	health_[2]->setName("increase");


	//-----------------------health pickups-------------------------
	//initialise membership functions for rule
	healthPickups_[0] = new Trapezoid;
	healthPickups_[1] = new Triangle;
	healthPickups_[2] = new Trapezoid;

	//set values for decrease rule
	healthPickups_[0]->setInterval(-1, 3);
	healthPickups_[0]->setMiddle(-1, 0);
	healthPickups_[0]->setType('r');
	healthPickups_[0]->setName("decrease");

	//set values for maintain rule
	healthPickups_[1]->setInterval(0, 6);
	healthPickups_[1]->setMiddle(3, 3);
	healthPickups_[1]->setType('t');
	healthPickups_[1]->setName("maintain");

	//set values for increase rule
	healthPickups_[2]->setInterval(3, 10);
	healthPickups_[2]->setMiddle(5, 10);
	healthPickups_[2]->setType('r');
	healthPickups_[2]->setName("increase");

	//-----------------defuzification-------------------
	sugendo_ = new Sugendo(0,0,0);
}

double Difficulty::getValue(float difficulty, int hazzards, int health, int healthPickups) {
	
	//set values to min/max if less/greater than them
	//else would return 0
	if (hazzards < HAZZARD_MIN) {
		hazzards = HAZZARD_MIN;
	}
	else if (hazzards > HAZZARD_MAX) {
		hazzards = HAZZARD_MAX;
	}

	if (health < HEALTH_MIN) {
		health = HEALTH_MIN;
	}
	else if (health > HEALTH_MAX) {
		health = HEALTH_MAX;
	}

	if (healthPickups < HEALTHPICKUP_MIN) {
		healthPickups = HEALTHPICKUP_MIN;
	}
	else if (healthPickups > HEALTHPICKUP_MAX) {
		healthPickups = HEALTHPICKUP_MAX;
	}

	//get values for decrease rules
	double decrease = hazzards_[0]->getValue(hazzards)
		+ health_[0]->getValue(health)
		+ healthPickups_[0]->getValue(healthPickups);
	//divide by number of rules to get average
	decrease = decrease / 3;

	//get values for maintain rules
	double maintain = hazzards_[1]->getValue(hazzards)
		+ health_[1]->getValue(health)
		+ healthPickups_[1]->getValue(healthPickups);
	//divide by number of rules to get average
	maintain = maintain / 3;

	//get values for increase rules
	double increase = hazzards_[2]->getValue(hazzards)
		+ health_[2]->getValue(health)
		+ healthPickups_[2]->getValue(healthPickups);
	//divide by number of rules to get average
	increase = increase / 3;

	//set values for increase, maintain and decrese 
	sugendo_->setValues(difficulty - 10, difficulty, difficulty + 10);

	//return defuzified value
	return sugendo_->getValue(decrease, maintain, increase);

}

Difficulty::~Difficulty() {
	for (int x = 0; x < 3; x++) {
		delete hazzards_[x];
		delete health_[x];
		delete healthPickups_[x];
	}
	delete sugendo_;
}

Coins::Coins() {
	//set membership functions for coin rules
	coins_[0] = new Trapezoid;
	coins_[1] = new Triangle;
	coins_[2] = new Trapezoid;

	//set values for  decrease rule 
	coins_[0]->setInterval(-1, 50);
	coins_[0]->setMiddle(-1, 25);
	coins_[0]->setType('r');
	coins_[0]->setName("decrease");

	//set values for maintain rule
	coins_[1]->setInterval(25, 75);
	coins_[1]->setMiddle(50, 50);
	coins_[1]->setType('t');
	coins_[1]->setName("maintain");

	//set values for increase rule
	coins_[2]->setInterval(50, 101);
	coins_[2]->setMiddle(75, 101);
	coins_[2]->setType('r');
	coins_[2]->setName("increase");

	sugendo_ = new Sugendo(0, 0, 0);
}

double Coins::getValue(int coins, float percentCollected) {

	//set values to min/max if less/greater than them
	//else would return 0
	if (percentCollected > 100)
	{
		percentCollected = 100;
	}
	if (percentCollected < 0)
	{
		percentCollected = 0;
	}

	//get value for decrease rule
	double decrease = coins_[0]->getValue(percentCollected);

	//get value for maintain rule
	double maintain = coins_[1]->getValue(percentCollected);

	//get value for increase rule
	double increase = coins_[2]->getValue(percentCollected);

	//set values for defuzification
	sugendo_->setValues(coins - 5, coins, coins + 5);

	//return defuzzified value
	return sugendo_->getValue(decrease, maintain, increase);

}

Coins::~Coins()
{
	delete sugendo_;
	delete coins_[0];
	delete coins_[1];
	delete coins_[2];
}