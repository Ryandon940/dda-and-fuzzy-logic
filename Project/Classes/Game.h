#ifndef GAME_H
#define GAME_H

#include "cocos2d.h"
#include <list>
#include "ProceduralGeneration.h"
#include "GameObject.h"
#include "DDA.h"
#include "FuzzyInferenceSystem.h"
#include "TXToutput.h"

#define HAZZARD_TIMEOUT 1.0f
#define HAZZARD_IMPACT 15
#define HEALTH_IMPACT 25
#define COIN_IMPACT 25
#define START_DIFFICULTY 20.0f
#define COIN_MIN 5
#define COIN_MAX 25


enum class PhysicsCategory {
	None = 0,
	Player = (0x1 << 0),    
	Wall = (0x1 << 1), 
	Coin = (0x1 << 2), 
	Hazzard = (0x1 << 3),
	Health = (0x1 << 4),
	DeathPlane = (0x1 <<5),
	All = PhysicsCategory::Player | PhysicsCategory::Wall | PhysicsCategory::Coin | PhysicsCategory::Hazzard | PhysicsCategory::Health  | PhysicsCategory::DeathPlane
};

class Game : public cocos2d::Scene
{
public:
	~Game();

    static cocos2d::Scene* createScene();

    virtual bool init();
	void resetGame();
	void update(float dt);
    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);
	void menuM1Callback(cocos2d::Ref* pSender);
	void menuM2Callback(cocos2d::Ref* pSender);

	
	void onKeyPrerssed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);
	void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);
	bool onContactBegan(PhysicsContact &contact);
	bool onContactEnd(PhysicsContact &contact);


    // implement the "static create()" method manually
    CREATE_FUNC(Game);

private:
	void buildBlock();
	GameObject* createBrick(Vec2 origin, int x, int y);
	GameObject*  createCoin(Vec2 origin, int x, int y);
	GameObject*  createHazzard(Vec2 origin, int x, int y);
	GameObject*  createHealth(Vec2 origin, int x, int y);
	GameObject*  createDeathPlane(Vec2 origin, int x, int y);

	int tileWidth;
	int tileHeight;

	std::list<cocos2d::EventKeyboard::KeyCode> KeysDown;

	ProceduralGeneration * generator;
	int block; //0 = blockA, 1 = blockB, 2 = blockC
	std::list<GameObject*> blockA;
	std::list<GameObject*> blockB;
	std::list<GameObject*> blockC;
	GameObject* player;
	int wallTouching;
	int jumpTimeout;
	int blockNum;

	bool fuzzy;
	float currentDifficulty;
	float otherDifficulty;
	int coins;
	int otherCoins;
	Difficulty *difficulty_;
	Coins *coins_;
	CrispDifficulty *crispDifficulty_;
	CrispCoins *crispCoins_;

	Output* output_;

	Menu* fuzzyButton;
	Menu* crispButton;
	bool gameRunning;

	int coinsCollectedToatl;
	CCLabelTTF *label;
	CCLabelTTF *labelCoins;
	CCLabelTTF *labelDifficulty;
	CCLabelTTF *labelOtherDifficulty;

	float labelXPos;
	int playerHealth;
	int oldHealth;
	int coinsCurrentBlock;
	int coinsNextBlock;
	int coinsCollected;
	float hazzardTimeout;
	int hazzardsTouching;
	int hazzardsTouched; //counter for times damage taken
	int healthToBeAdded;
	int healthCollected; //dda
};

#endif
