#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H
#include "cocos2d.h"
USING_NS_CC;

enum Type {
	Wall,
	Player,
	Hazzard,
	Coin,
	Health,
	KillPlane,
	Empty
};

class GameObject{
public:
	GameObject(const std::string& filename, Type t);
	void update();
	inline void deactivate() { active_ = false; }
	Vec2 velocity_;
	Type type_;
	bool active_;
	Vec2 size_;
	Sprite* sprite;
	bool grounded;
};

#endif 
