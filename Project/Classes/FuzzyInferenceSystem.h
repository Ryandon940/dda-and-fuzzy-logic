#ifndef FUZZYINFERENCESYSTEM_H
#define FUZZYINFERENCESYSTEM_H

#include "FuzzyLogic.h"

#define HAZZARD_MIN -1
#define HAZZARD_MAX 10

#define HEALTH_MIN -1
#define HEALTH_MAX 100

#define HEALTHPICKUP_MIN 0
#define HEALTHPICKUP_MAX 10

#define COINS_MIN -1
#define COINS_MAX 100

class Difficulty {
public:
	Difficulty();
	~Difficulty();
	double getValue(float difficulty, int hazzards, int health, int healthPickups);
private:
	FuzzyFunction *hazzards_[3];
	FuzzyFunction *health_[3];
	FuzzyFunction *healthPickups_[3];
	//FuzzyFunction *time_[3];
	Sugendo *sugendo_;
};

class Coins {
public:
	Coins();
	~Coins();
	double getValue(int coins, float percentCollected);
private:
	FuzzyFunction *coins_[3];
	Sugendo * sugendo_;
};

#endif // !FUZZYINFERENCESYSTEM_H
