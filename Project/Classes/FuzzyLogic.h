#ifndef FUZZYLOGIC_H
#define FUZZYLOGIC_H

#include <iostream>
#include <cmath>
#include <cstring>

class FuzzyFunction
{
protected:
	double dLeft, dRight;
	char   cType;
	char*  sName;

public:
	FuzzyFunction() {};
	virtual ~FuzzyFunction() { delete[] sName; sName = NULL; }

	virtual void setInterval(double l, double r);
	virtual void setMiddle(double dL = 0, double dR = 0) = 0;
	inline virtual void setType(char c) {	cType = c;}
	virtual void setName(const char* s);
	bool isDotInInterval(double t);
	inline char getType(void)const { return cType; }
	inline char* getName() const{ return sName;}
	virtual double getValue(double t) = 0;
};

class Triangle : public FuzzyFunction
{
private:
	double dMiddle;

public:
	inline  void setMiddle(double dL, double dR){ dMiddle = dL;}
	double getValue(double t);
};

class Trapezoid: public FuzzyFunction
{
private:
	double dLeftMiddle, dRightMiddle;

public:
	void setMiddle(double dL, double dR);
	double getValue(double t);
};

class Sugendo {
public:
	Sugendo(double low, double med, double max);
	void setValues(double low, double med, double max);
	double getValue(double low, double med, double max);
private:
	double low_;
	double med_;
	double max_; 
};

#endif