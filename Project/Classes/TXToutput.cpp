#include "TXToutput.h"

Output::Output(char* filename) {
	//empty and open file for output values
	remove(filename);
	file_ = fopen(filename, "a");
}

void Output::AddOutput(float value) {
	//push float to file
	fflush(file_);
	fprintf(file_,"%f\n", value);
}
void Output::AddChar(char c) {
	//push char to file
	fflush(file_);
	fprintf(file_, &c);
	fprintf(file_, "\n");
}

Output::~Output() {
	fclose(file_);
	delete file_; 
}
