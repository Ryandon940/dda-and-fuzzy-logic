#ifndef DDA_H
#define DDA_H

class CrispDifficulty {
public:
	CrispDifficulty();
	double getValue(float difficulty, int hazzards, int health, int healthPickups);
private:
	float hazzardsLow, hazzardsHigh;
	float healthLow, healthHigh;
	float healthPickupLow, healthPickupHigh;
};

class CrispCoins {
public:
	CrispCoins();
	double getValue(int coins, float percentCollected);
private:
	float low, high;
};

#endif // !DDA_H
#pragma once
