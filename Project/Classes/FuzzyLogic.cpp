#include "FuzzyLogic.h"

void FuzzyFunction::setInterval(double l, double r) {
	//set left and right values for membership function
	dLeft = l; dRight = r;
}

void FuzzyFunction::setName(const char* s){
	//set name of function
	sName = new char[strlen(s) + 1];
	strcpy(sName, s);
}

bool FuzzyFunction::isDotInInterval(double t){
	//return true if input value is in range of membership function
	if ((t >= dLeft) && (t <= dRight)) return true; else return false;
}


double Triangle::getValue(double t)
{
	//calculate and return membership value from input value
	if (t <= dLeft)		//less than min left value
		return 0;
	else if (t < dMiddle)		//less than middle value
		return (t - dLeft) / (dMiddle - dLeft);
	else if (t == dMiddle)		//equlas middle value
		return 1.0;
	else if (t < dRight)		//less than max right value
		return (dRight - t) / (dRight - dMiddle);
	else
		return 0;
}

void Trapezoid::setMiddle(double dL, double dR)
{
	//set middle
	dLeftMiddle = dL; dRightMiddle = dR;
}

double Trapezoid::getValue(double t)
{
	//calculate and return membership value from input value
	if (t <= dLeft)		//less than minimum left value
		return 0;		
	else if (t < dLeftMiddle)		//less than left of middle
		return (t - dLeft) / (dLeftMiddle - dLeft);
	else if (t <= dRightMiddle)			// less than right of middle
		return 1.0;
	else if (t < dRight)		//less than max right value
		return (dRight - t) / (dRight - dRightMiddle);
	else
		return 0;
}

Sugendo::Sugendo(double low, double med, double max) {
	//init values
	low_ = low;
	med_ = med;
	max_ = max;
}

void Sugendo::setValues(double low, double med, double max) {
	//set values
	low_ = low;
	med_ = med;
	max_ = max;
}


double Sugendo::getValue(double low, double med, double max) {
	//calculate output value for sugeno defuzification
	return (low * low_ + med * med_ + max * max_) / (low + med + max);

}