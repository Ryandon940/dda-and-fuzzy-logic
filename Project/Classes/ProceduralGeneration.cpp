#include "ProceduralGeneration.h"

void ProceduralGeneration::GenerateLevel(float difficulty, float coinProbability){
	//loop through map
	for (int x = 0; x < MAP_WIDTH; x++) {
		for (int y = 0; y < MAP_HEIGHT; y++) {
			//switch case on y of map
			switch (y)
			{
			case (MAP_HEIGHT -1):	//place wall along top of map
				map[x][y] = Wall;
				break;
			case PLATFORM1:		//place walls at the height of the platforms
			case PLATFORM2:
			case PLATFORM3:
				map[x][y] = Wall;
				break;
			case 0:		//kill plane along bottom of map
				map[x][y] = KillPlane;
				break;
			default:	//every other square empty
				map[x][y] = Empty;
				break;

			};
		}
	}

	//calculate the probability for gaps, pickups and hazards
	int gap1count = (MAP_WIDTH * PLATFORM1_PROBABILITY) / 100 * difficulty;
	int gap2count = (MAP_WIDTH * PLATFORM2_PROBABILITY) / 100 * difficulty;
	int gap3count = (MAP_WIDTH * PLATFORM3_PROBABILITY) / 100 * difficulty;
	int coinCount = coinProbability;
	int hazzardCount = (MAP_WIDTH * MAP_HEIGHT) / 10;
	hazzardCount = float(hazzardCount)/ 100 * difficulty;
	int healthCount = ((MAP_WIDTH * MAP_HEIGHT) / 40);
	healthCount = float(healthCount)/ 100 * (100 - difficulty);


	int count = 0;
	//randomly place gaps for platform 1
	do {
		int xPos = rand() % MAP_WIDTH;
		if (map[xPos][PLATFORM1] == Wall) {
			map[xPos][PLATFORM1] = Empty;
			count++;
		}
	} while (gap1count > count);

	count = 0;
	//randomly place gaps for platform 2
	do {
		int xPos = rand() % MAP_WIDTH;
		if (map[xPos][PLATFORM2] == Wall) {
			map[xPos][PLATFORM2] = Empty;
			count++;
		}
	} while (gap2count > count);

	count = 0;
	//randomly place gaps for platform 3
	do {
		int xPos = rand() % MAP_WIDTH;
		if (map[xPos][PLATFORM3] == Wall) {
			map[xPos][PLATFORM3] = Empty;
			count++;
		}
	} while (gap3count > count);

	count = 0;
	//reandomly place coins
	do {
		int xPos = rand() % MAP_WIDTH;
		int yPos = rand() % MAP_HEIGHT;
		if (map[xPos][yPos] == Empty) {
			map[xPos][yPos] = Coin;
			count++;
		}
	} while (coinCount > count);

	count = 0;
	//randomly place hazards
	do {
		int xPos = rand() % MAP_WIDTH;
		int yPos = rand() % MAP_HEIGHT;
		if (map[xPos][yPos] == Empty) {
			map[xPos][yPos] = Hazzard;
			count++;
		}
	} while (hazzardCount > count);

	count = 0;
	//randomly place health pickups
	do {
		int xPos = rand() % MAP_WIDTH;
		int yPos = rand() % MAP_HEIGHT;
		if (map[xPos][yPos] == Empty) {
			map[xPos][yPos] = Health;
			count++;
		}
	} while (healthCount > count);

}