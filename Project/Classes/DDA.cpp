#include "DDA.h"

CrispDifficulty::CrispDifficulty() {
	//define crossover points for static rule set
	healthLow = 25;
	healthHigh = 75;
	hazzardsLow = 1.5;
	hazzardsHigh = 4.5;
	healthPickupLow = 1.5;
	healthPickupHigh = 4.5;
}

double CrispDifficulty::getValue(float difficulty, int hazzards, int health, int healthPickups) {

	double returnValue = 0;

	//check if health is lower or greater than crossover points and increment/decrement return value
	if (health < healthLow) {
		returnValue -= 10;
	}
	else if(health>healthHigh){
		returnValue += 10;
	}

	//check if hazzards is lower or greater than crossover points and increment/decrement return value
	if (hazzards < hazzardsLow) {
		returnValue += 10;
	}
	else if (hazzards>hazzardsHigh) {
		returnValue -= 10;
	}

	//check if health pickups is lower or greater than crossover points and increment/decrement return value
	if (healthPickups < healthPickupLow) {
		returnValue -= 10;
	}
	else if (healthPickups>healthPickupHigh) {
		returnValue += 10;
	}

	//divide the return value by the number of rules
	returnValue = returnValue / 3;

	//return the new difficulty
	return difficulty + returnValue;

}

CrispCoins::CrispCoins() {
	low = 37.5;
	high = 62.5;
}

double CrispCoins::getValue(int coins, float percentCollected) {

	if (percentCollected < low) {
		return coins - 5;
	}
	else if (percentCollected > high) {
		return coins + 5;
	}
	else {
		return coins;
	}

}
	
