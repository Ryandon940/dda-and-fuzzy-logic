#ifndef PROCEDURAL_GENERATION_H
#define PROCEDURAL_GENERATION_H

#include <list>
#include "GameObject.h"

#define MAP_WIDTH 20
#define MAP_HEIGHT 14
#define PLATFORM1 1
#define PLATFORM1_PROBABILITY 0.40
#define PLATFORM2 5
#define PLATFORM2_PROBABILITY 0.60
#define PLATFORM3 9
#define PLATFORM3_PROBABILITY 0.75



class ProceduralGeneration {
public:
	ProceduralGeneration() {};
	~ProceduralGeneration() {};
	void GenerateLevel(float difficulty, float coinProbability);
	Type map[MAP_WIDTH][MAP_HEIGHT];
};

#endif 
