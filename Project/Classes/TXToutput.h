#ifndef TXTO_H
#define TXTO_H

#include <stdio.h>
#include <conio.h>

class Output {
public:
	Output(char* filename);
	~Output();
	void AddOutput(float value);
	void AddChar(char c);
private:
	FILE * file_;
	char* filename_;
};

#endif // !TXTO_H
#pragma once
