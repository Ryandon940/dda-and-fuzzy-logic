#include "Game.h"
#include "SimpleAudioEngine.h"
#include <sstream>

#define SSTR( x ) static_cast< std::ostringstream & >( \
        ( std::ostringstream() << std::dec << x ) ).str()

USING_NS_CC;

Scene* Game::createScene()
{
	//create 2D scene with gravity
	auto scene = Game::createWithPhysics();
	scene->getPhysicsWorld()->setGravity(Vec2(0, -100));
	//scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);
	auto layer = Game::create();
	scene->addChild(layer);
	return scene;
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
}

// on "init" you need to initialize your instance
bool Game::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }

    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    //create metghod 1 button with callback for when pressed
    auto M1 = MenuItemImage::create(
                                           "M1.png",
                                           "M1.png",
                                           CC_CALLBACK_1(Game::menuM1Callback, this));

    if (M1 == nullptr ||
		M1->getContentSize().width <= 0 ||
		M1->getContentSize().height <= 0)
    {
        problemLoading("'M1.png'");
    }
    else
    {
		//if loaded correctly set position
        float x = origin.x - visibleSize.width/10;
        float y = origin.y + visibleSize.height/2;
		M1->setPosition(Vec2(x,y));
	}

	//create method 2 button with callback for when pressed
	auto M2 = MenuItemImage::create(
		"M2.png",
		"M2.png",
		CC_CALLBACK_1(Game::menuM2Callback, this));

	if (M2 == nullptr ||
		M2->getContentSize().width <= 0 ||
		M2->getContentSize().height <= 0)
	{
		problemLoading("'M2.png'");
	}
	else
	{
		//if loaded correctly set position
		float x = origin.x + visibleSize.width / 4;
		float y = origin.y + visibleSize.height / 2;
		M2->setPosition(Vec2(x, y));
	}

	//add listeners for keyboard input
	auto keyboardEventListener = EventListenerKeyboard::create();
	keyboardEventListener->onKeyPressed = CC_CALLBACK_2(Game::onKeyPrerssed, this);
	keyboardEventListener->onKeyReleased = CC_CALLBACK_2(Game::onKeyReleased, this);
	this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(keyboardEventListener, this);

	//label for player health
	auto health = Label::create();

	//add button for fuzzy logic
	fuzzyButton = Menu::create(M1, NULL);
	fuzzyButton->setPosition(Vec2::ZERO);
	this->addChild(fuzzyButton, 1);

	//add button for crisp
	crispButton = Menu::create(M2, NULL);
	crispButton->setPosition(Vec2::ZERO);
	this->addChild(crispButton, 1);

	//scedule update funtion to run every frame
	this->schedule(schedule_selector(Game::update), 0);
	
	//initialise blocks
	blockA = std::list<GameObject*>();
	blockB = std::list<GameObject*>();
	blockC = std::list<GameObject*>();
	block = 0;
	generator = new ProceduralGeneration;

	//for mapbuilding calculations
	tileWidth = visibleSize.width / MAP_WIDTH;
	tileHeight = visibleSize.height / MAP_HEIGHT - 1;
	
	//init game variables
	coins = 10;
	otherCoins = 10;
	wallTouching = 0;
	jumpTimeout = 0;
	coinsCurrentBlock = 0;
	coinsNextBlock = 0;
	currentDifficulty = START_DIFFICULTY;
	otherDifficulty = START_DIFFICULTY;

	//init DDA
	coins_ = new Coins();
	difficulty_ = new Difficulty();
	crispCoins_ = new CrispCoins();
	crispDifficulty_ = new CrispDifficulty();

	//build first two blocks
	blockNum = 0;
	buildBlock();
	blockNum++;
	buildBlock();

	//init varibales used by DDA
	playerHealth = 100;
	oldHealth = 100;
	hazzardTimeout = HAZZARD_TIMEOUT;
	hazzardsTouching = 0;
	hazzardsTouched = 0;
	healthToBeAdded = 0;

	//create player
	player = new GameObject("pops.png", Player);
	Vec2 scale = Vec2(tileWidth / player->sprite->getContentSize().width, tileHeight / player->sprite->getContentSize().height);
	player->sprite->setScale(scale.x*0.75, scale.y * 1.5);
	player->sprite->setPosition(tileWidth*1.75, tileHeight*3.5);
	auto physicsBody = PhysicsBody::createBox(Size(player->sprite->getContentSize()),
	PhysicsMaterial(0.1f, 0.1f, 0.1f));
	physicsBody->setDynamic(true);
	physicsBody->setRotationEnable(false);

	//set player bitmasks to interact with other objects
	physicsBody->setCategoryBitmask((int)PhysicsCategory::Player);
	physicsBody->setCollisionBitmask((int)PhysicsCategory::Wall);
	physicsBody->setContactTestBitmask((int)PhysicsCategory::All);
	
	//add to scene
	player->sprite->setPhysicsBody(physicsBody);
	this->addChild(player->sprite);
	KeysDown = std::list<cocos2d::EventKeyboard::KeyCode>();

	//add listeners for physic contact starting and ending
	auto contactListener = EventListenerPhysicsContact::create();
	contactListener->onContactBegin = CC_CALLBACK_1(Game::onContactBegan, this);
	this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(contactListener, this);

	auto contactEndListener = EventListenerPhysicsContact::create();
	contactEndListener->onContactSeparate = CC_CALLBACK_1(Game::onContactEnd, this);
	this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(contactEndListener, this);

	//initialise and set positions fort health and difficulty labels
	float labelXPos = (origin.x + visibleSize.width -16)/2;
	float y = origin.y + visibleSize.height - 16;
	label = CCLabelTTF::create(SSTR(playerHealth), "arial", 12, Size(32, 32));
	label->setPosition(Vec2(labelXPos, y));
	this->addChild(label, 1);

	coinsCollectedToatl = 0;

	labelCoins = CCLabelTTF::create(SSTR(coinsCollectedToatl), "arial", 12, Size(32, 32));
	labelCoins->setPosition(Vec2(labelXPos, y - 16));
	this->addChild(labelCoins, 1);

	labelDifficulty = CCLabelTTF::create(SSTR(currentDifficulty), "arial", 12, Size(32, 32));
	labelDifficulty->setPosition(Vec2(labelXPos - visibleSize.width/3, y));
	this->addChild(labelDifficulty, 1);

	labelOtherDifficulty = CCLabelTTF::create(SSTR(labelOtherDifficulty), "arial", 12, Size(32, 32));
	labelOtherDifficulty->setPosition(Vec2(labelXPos + visibleSize.width / 3, y));
	this->addChild(labelOtherDifficulty, 1);
	gameRunning = false;

	//initialise text output and open file
	output_ = new Output("Values.txt");

    return true;
}

void Game::update(float dt) {
	player->velocity_ = Vec2(0, 0);
	
	//if game runnning - a method has been set
	if (gameRunning) {
		jumpTimeout--;
		//iterate through key presses
		for (std::list<cocos2d::EventKeyboard::KeyCode>::iterator iterator = KeysDown.begin(), end = KeysDown.end(); iterator != end; ++iterator)
		{
			//switch through arrow pressses and add corresponding velocity to player
			switch (*iterator) {
			case cocos2d::EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
				player->velocity_.x += 50;
				break;
			case cocos2d::EventKeyboard::KeyCode::KEY_LEFT_ARROW:
				player->velocity_.x -= 50;
				break;
			case cocos2d::EventKeyboard::KeyCode::KEY_UP_ARROW:
				if (wallTouching > 0 && jumpTimeout < 1 && player->sprite->getPhysicsBody()->getVelocity().y < 1) {
					player->velocity_.y += 150;
					jumpTimeout = 15;
				}
				break;
			case cocos2d::EventKeyboard::KeyCode::KEY_DOWN_ARROW:
				player->velocity_.y -= 15;
				break;

			}
		}

		//if player has reached the end of a blovck
		if (player->sprite->getPosition().x >= Director::getInstance()->getVisibleSize().width *blockNum)
		{
			blockNum++;
			//calculate the percent of coins collected
			float perecent = 100 / float(coinsCurrentBlock) * float(coinsCollected);
			
			//mke sure percent not over or under 100
			if (coins < COIN_MIN) { coins = COIN_MIN; }
			if (coins > COIN_MAX) { coins = COIN_MAX; }
			
			//if FIS chosed
			if (fuzzy) {
				//calculate DDA values
				coins = coins_->getValue(coins, perecent);
				otherCoins = crispCoins_->getValue(otherCoins, perecent);
				currentDifficulty = difficulty_->getValue(currentDifficulty, hazzardsTouched, playerHealth, healthCollected);
				otherDifficulty = crispDifficulty_->getValue(otherDifficulty, hazzardsTouched, playerHealth, healthCollected);
				
				//save values to file
				output_->AddChar('f');
				output_->AddOutput(currentDifficulty);
				output_->AddOutput(coins);
				
				output_->AddChar('c');
				output_->AddOutput(otherDifficulty);
				output_->AddOutput(otherCoins);

			}
			else {
				//calculate dda values
				coins = crispCoins_->getValue(coins, perecent);
				otherCoins = coins_->getValue(otherCoins, perecent);
				currentDifficulty = crispDifficulty_->getValue(currentDifficulty, hazzardsTouched, playerHealth, healthCollected);
				otherDifficulty = difficulty_->getValue(otherDifficulty, hazzardsTouched, playerHealth, healthCollected);
				
				//save to file
				output_->AddChar('c');
				output_->AddOutput(currentDifficulty);
				output_->AddOutput(coins);

				output_->AddChar('f');
				output_->AddOutput(otherDifficulty);
				output_->AddOutput(otherCoins);
			}
			buildBlock();
			coinsCollected = 0;
		}

		//remove health is hazzard touching and timeout time has passed
		if (hazzardsTouching > 0 && hazzardTimeout >= HAZZARD_TIMEOUT)
		{
			playerHealth -= HAZZARD_IMPACT;
			hazzardTimeout = 0;
			hazzardsTouched++;
		}

		//add health if a pickup was collected
		if (healthToBeAdded > 0) {
			playerHealth += healthToBeAdded * HEALTH_IMPACT;
			healthToBeAdded = 0;
			if (playerHealth > 100) {
				playerHealth = 100;
			}
		}

		//reset when players health drops bellow 1
		if (playerHealth < 1) {
			resetGame();
		}
		
		hazzardTimeout += dt;
}
		//set the positions of all UI to match players new position so it will follow the camera
		auto visibleSize = Director::getInstance()->getVisibleSize();
		auto ws = player->sprite->getParent()->convertToWorldSpace(player->sprite->getPosition());
		this->getDefaultCamera()->setCameraFlag(CameraFlag::USER5);
		this->setPosition(this->getPosition().x - ws.x + Director::getInstance()->getWinSize().width / 2, this->getPosition().y);
		label->setString(SSTR(playerHealth));
		label->setPosition(labelXPos + player->sprite->getPosition().x, label->getPosition().y);
		labelCoins->setString(SSTR(coinsCollectedToatl));
		labelCoins->setPosition(labelXPos + player->sprite->getPosition().x, labelCoins->getPosition().y);
		labelDifficulty->setString(SSTR(currentDifficulty));
		labelDifficulty->setPosition(Vec2(labelXPos - visibleSize.width / 3 + player->sprite->getPosition().x, labelDifficulty->getPosition().y));
		labelOtherDifficulty->setString(SSTR(otherDifficulty));
		labelOtherDifficulty->setPosition(Vec2(labelXPos + visibleSize.width / 3 + player->sprite->getPosition().x, labelOtherDifficulty->getPosition().y));
		
		//apply veloicty to player
		player->update();
}

void Game::menuCloseCallback(Ref* pSender)
{
    //Close the cocos2d-x game scene and quit the application
    Director::getInstance()->end();

    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}

void Game::menuM1Callback(cocos2d::Ref* pSender) {
	//start game with FIS and hide buttons
	fuzzy = true;
	gameRunning = true;
	fuzzyButton->setVisible(false);
	crispButton->setVisible(false);
}

void Game::menuM2Callback(cocos2d::Ref* pSender) {
	//start game with non fuzzy DDA and hide buttons
	fuzzy = false;
	gameRunning = true;
	fuzzyButton->setVisible(false);
	crispButton->setVisible(false);
}

void Game::buildBlock() {

	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	origin.x += visibleSize.width * blockNum;
	
	//resest values
	hazzardsTouched = 0;
	healthCollected = 0;
	oldHealth = playerHealth;

	//generate 2D array of map for current difficulty
	generator->GenerateLevel(currentDifficulty, coins);

	//store coins in block for calculations
	coinsCurrentBlock = coinsNextBlock;
	coinsNextBlock = 0;
	std::list<GameObject*>* currentBlock;

	//if game has just started clear out all lists
	if (blockNum == 0) {
		for (std::list<GameObject*>::iterator it = blockA.begin(); it != blockA.end(); ++it) {

			(*it)->sprite->removeFromParentAndCleanup(true);
			delete(*it);
		}
		for (std::list<GameObject*>::iterator it = blockB.begin(); it != blockB.end(); ++it) {

			(*it)->sprite->removeFromParentAndCleanup(true);
			delete(*it);
		}
		for (std::list<GameObject*>::iterator it = blockC.begin(); it != blockC.end(); ++it) {

			(*it)->sprite->removeFromParentAndCleanup(true);
			delete(*it);
		}
		blockA.clear();
		blockB.clear();
		blockC.clear();
	}

	//if current block is 0 clear out third block and fill with array
	if (block == 0)
	{
		//clear old block here
		block = 1;
		currentBlock = &blockC;
		for (std::list<GameObject*>::iterator it = blockC.begin(); it != blockC.end(); ++it) {
			(*it)->sprite->removeFromParentAndCleanup(true);

			delete(*it);
		}
		blockC.clear();
	}
	else if (block == 1)  //if current block is 1 clear out first block and fill with array
	{
		//clear old block here
		block = 2;
		currentBlock = &blockA;
		for (std::list<GameObject*>::iterator it = blockA.begin(); it != blockA.end(); ++it) {

			(*it)->sprite->removeFromParentAndCleanup(true);


			delete(*it);
		}
		blockA.clear();
	}
	else {   //if current block is 3 clear out second block and fill with array
		//clear old block here
		block = 0;
		currentBlock = &blockB;
		for (std::list<GameObject*>::iterator it = blockB.begin(); it != blockB.end(); ++it) {

			(*it)->sprite->removeFromParentAndCleanup(true);
			delete(*it);
		}
		blockB.clear();
	}


	int tileWidth = visibleSize.width / MAP_WIDTH;
	int tileHeight = visibleSize.height / MAP_HEIGHT - 1;

	//calculate offset from origin for surrent block
	if (blockNum > 0) {
		origin.x -= (tileWidth / 2)*blockNum;
		}
	//loop through map
	for (int x = 0; x < MAP_WIDTH; x++) {
		for (int y = 0; y < MAP_HEIGHT; y++) {
			if (blockNum == 0 && x == 0)
			{
				currentBlock->push_back(createBrick(origin,x-1,y)); //build brick at start of first block so player cant fall off
			}
			//switch through array and build the tiles at each position
			switch (generator->map[x][y])
			{
			case Wall:
				currentBlock->push_back(createBrick(origin, x, y));
				break;
			case Coin:
				currentBlock->push_back(createCoin(origin, x, y));
				coinsNextBlock++;
				break;
			case Hazzard:
				currentBlock->push_back(createHazzard(origin, x, y));
				break;
			case Health:
				currentBlock->push_back(createHealth(origin, x, y));
				break;
			case KillPlane:
				currentBlock->push_back(createDeathPlane(origin, x, y-1));
				break;
			};
		}
	}

}

void  Game::onKeyPrerssed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
	bool found = false;
	//itterate through current key presses
	for (std::list<cocos2d::EventKeyboard::KeyCode>::iterator iterator = KeysDown.begin(), end = KeysDown.end(); iterator != end; ++iterator) {
		//if key was already found to be pressed set found to true
		if (*iterator == keyCode)
		{
			found = true;
		}
	}
	//if key was not already found to be pressed add to list of key presses
	if (!found)
	{
		KeysDown.push_back(keyCode);
	}
}
void Game::onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
	//iterate through key presses
	for (std::list<cocos2d::EventKeyboard::KeyCode>::iterator iterator = KeysDown.begin(), end = KeysDown.end(); iterator != end; ++iterator)
	{
		//remove all intsnaces of that key
		if (*iterator == keyCode)
		{
			iterator = KeysDown.erase(iterator);
			break;
		}
	}
}

bool Game::onContactBegan(PhysicsContact &contact) {
	//if shape A is a Player
	if (contact.getShapeA()->getCategoryBitmask() == (int)PhysicsCategory::Player) {
		//if colliding with wall
		if (contact.getShapeB()->getCategoryBitmask() == (int)PhysicsCategory::Wall) {
			wallTouching++;
		}

		//if colliding with coin
		if (contact.getShapeB()->getCategoryBitmask() == (int)PhysicsCategory::Coin)
		{
			//remove coin
			contact.getShapeB()->getBody()->getNode()->getPhysicsBody()->setEnabled(false);
			contact.getShapeB()->getBody()->getNode()->setVisible(false);
			coinsCollected++;
			coinsCollectedToatl++;
		}

		//if collisidng with hazzard
		if (contact.getShapeB()->getCategoryBitmask() == (int)PhysicsCategory::Hazzard)
		{
			hazzardsTouching++;
		}

		//if colliding with health pickup
		if (contact.getShapeB()->getCategoryBitmask() == (int)PhysicsCategory::Health)
		{
			//remove health pickup
			contact.getShapeB()->getBody()->getNode()->getPhysicsBody()->setEnabled(false);
			contact.getShapeB()->getBody()->getNode()->setVisible(false);
			healthCollected++;
			healthToBeAdded++;
		}

		//if colliding with death plane kill player
		if (contact.getShapeB()->getCategoryBitmask() == (int)PhysicsCategory::DeathPlane)
		{
			playerHealth = 0;
		}
	}

	//if shape B is player
	if (contact.getShapeB()->getCategoryBitmask() == (int)PhysicsCategory::Player) {
		//if colliding with wall
		if (contact.getShapeA()->getCategoryBitmask() == (int)PhysicsCategory::Wall) {
			wallTouching++;
		}

		//if colliding with  coin
		if (contact.getShapeA()->getCategoryBitmask() == (int)PhysicsCategory::Coin)
		{
			//remove coin
			contact.getShapeA()->getBody()->getNode()->getPhysicsBody()->setEnabled(false);
			contact.getShapeA()->getBody()->getNode()->setVisible(false);
			coinsCollected++;
			coinsCollectedToatl++;
		}

		//if colliding with hazzard
		if (contact.getShapeA()->getCategoryBitmask() == (int)PhysicsCategory::Hazzard)
		{
			hazzardsTouching++;
		}

		//if colliding with health pickup
		if (contact.getShapeA()->getCategoryBitmask() == (int)PhysicsCategory::Health)
		{
			//remove pickup
			contact.getShapeA()->getBody()->getNode()->getPhysicsBody()->setEnabled(false);
			contact.getShapeA()->getBody()->getNode()->setVisible(false);
			healthCollected++;
			healthToBeAdded++;
		}

		//if colliding with death plane kill player
		if (contact.getShapeA()->getCategoryBitmask() == (int)PhysicsCategory::DeathPlane)
		{
			playerHealth = 0;
		}
	}
	return true;
}

bool Game::onContactEnd(PhysicsContact &contact) {

	//check if player left contact with wall or hazzard and decrement counters
	if (contact.getShapeA()->getCategoryBitmask() == (int)PhysicsCategory::Player) {
		if (contact.getShapeB()->getCategoryBitmask() == (int)PhysicsCategory::Wall) {
			wallTouching--;
		}
		if (contact.getShapeB()->getCategoryBitmask() == (int)PhysicsCategory::Hazzard)
		{
			hazzardsTouching--;
		}
	}


	if (contact.getShapeB()->getCategoryBitmask() == (int)PhysicsCategory::Player) {
		if (contact.getShapeA()->getCategoryBitmask() == (int)PhysicsCategory::Wall) {
			wallTouching--;
		}
		if (contact.getShapeA()->getCategoryBitmask() == (int)PhysicsCategory::Hazzard)
		{
			hazzardsTouching--;
		}
	}
	return true;
}

GameObject*  Game::createBrick(Vec2 origin, int x, int y) {

	//create brick
	GameObject* obj = new GameObject("brick.jpg", generator->map[x][y]);
	Vec2 scale = Vec2(tileWidth / obj->sprite->getContentSize().width, tileHeight / obj->sprite->getContentSize().height);
	obj->sprite->setScale(scale.x, scale.y);
	obj->size_ = Vec2(obj->sprite->getContentSize().width * scale.x, obj->sprite->getContentSize().height  * scale.y);
	Vec2 pos = Vec2((origin.x + x * obj->size_.x), (origin.y + y * obj->size_.y));
	obj->sprite->setPosition(pos);
	auto physicsBody = PhysicsBody::createBox(Size(obj->sprite->getContentSize()),
		PhysicsMaterial(0.1f, 1.0f, 0.0f));

	//create physics body
	physicsBody->setCategoryBitmask((int)PhysicsCategory::Wall);
	physicsBody->setCollisionBitmask((int)PhysicsCategory::Player);
	physicsBody->setContactTestBitmask((int)PhysicsCategory::Player);

	//add to scene
	physicsBody->setDynamic(false);
	obj->sprite->setPhysicsBody(physicsBody);
	this->addChild(obj->sprite, 0);
	return obj;

}

GameObject*  Game::createCoin(Vec2 origin, int x, int y) {

	//create coin
	GameObject* obj = new GameObject("coin1.png", generator->map[x][y]);
	Vec2 scale = Vec2(tileWidth / obj->sprite->getContentSize().width, tileHeight / obj->sprite->getContentSize().height);
	obj->sprite->setScale(scale.x *0.75, scale.y *0.75);
	obj->size_ = Vec2(obj->sprite->getContentSize().width * scale.x, obj->sprite->getContentSize().height  * scale.y);
	Vec2 pos = Vec2((origin.x + x * obj->size_.x), (origin.y + y * obj->size_.y));
	obj->sprite->setPosition(pos);
	auto physicsBody = PhysicsBody::createBox(Size(obj->sprite->getContentSize()),
		PhysicsMaterial(0.1f, 1.0f, 0.0f));

	//create physics body
	physicsBody->setCategoryBitmask((int)PhysicsCategory::Coin);
	//physicsBody->setCollisionBitmask((int)PhysicsCategory::Player);
	physicsBody->setContactTestBitmask((int)PhysicsCategory::Player);

	//add to scene
	physicsBody->setDynamic(false);
	obj->sprite->setPhysicsBody(physicsBody);
	this->addChild(obj->sprite, 0);
	return obj;
}

GameObject*  Game::createHazzard(Vec2 origin, int x, int y) {

	//create hazzard
	GameObject* obj = new GameObject("fire.png", generator->map[x][y]);
	Vec2 scale = Vec2(tileWidth / obj->sprite->getContentSize().width, tileHeight / obj->sprite->getContentSize().height);
	obj->sprite->setScale(scale.x *0.75, scale.y *0.75);
	obj->size_ = Vec2(obj->sprite->getContentSize().width * scale.x, obj->sprite->getContentSize().height  * scale.y);
	Vec2 pos = Vec2((origin.x + x * obj->size_.x), (origin.y + y * obj->size_.y));
	obj->sprite->setPosition(pos);
	auto physicsBody = PhysicsBody::createBox(Size(obj->sprite->getContentSize()),
		PhysicsMaterial(0.1f, 1.0f, 0.0f));

	//create physics body
	physicsBody->setCategoryBitmask((int)PhysicsCategory::Hazzard);
	physicsBody->setContactTestBitmask((int)PhysicsCategory::Player);

	//add to scene
	physicsBody->setDynamic(false);
	obj->sprite->setPhysicsBody(physicsBody);
	this->addChild(obj->sprite, 0);
	return obj;
}

GameObject*  Game::createHealth(Vec2 origin, int x, int y) {

	//create physics body
	GameObject* obj = new GameObject("heart.png", generator->map[x][y]);
	Vec2 scale = Vec2(tileWidth / obj->sprite->getContentSize().width, tileHeight / obj->sprite->getContentSize().height);
	obj->sprite->setScale(scale.x *0.75, scale.y *0.75);
	obj->size_ = Vec2(obj->sprite->getContentSize().width * scale.x, obj->sprite->getContentSize().height  * scale.y);
	Vec2 pos = Vec2((origin.x + x * obj->size_.x), (origin.y + y * obj->size_.y));
	obj->sprite->setPosition(pos);
	auto physicsBody = PhysicsBody::createBox(Size(obj->sprite->getContentSize()),
		PhysicsMaterial(0.1f, 1.0f, 0.0f));

	//create physics body
	physicsBody->setCategoryBitmask((int)PhysicsCategory::Health);
	physicsBody->setContactTestBitmask((int)PhysicsCategory::Player);

	//add to scene
	physicsBody->setDynamic(false);
	obj->sprite->setPhysicsBody(physicsBody);
	this->addChild(obj->sprite, 0);
	return obj;
}

GameObject* Game::createDeathPlane(Vec2 origin, int x, int y) {
	
	//create death plane
	GameObject* obj = new GameObject("death.jpg", generator->map[x][y]);
	Vec2 scale = Vec2(tileWidth / obj->sprite->getContentSize().width, tileHeight / obj->sprite->getContentSize().height);
	obj->sprite->setScale(scale.x *0.75, scale.y *0.75);
	obj->size_ = Vec2(obj->sprite->getContentSize().width * scale.x, obj->sprite->getContentSize().height  * scale.y);
	Vec2 pos = Vec2((origin.x + x * obj->size_.x), (origin.y + y * obj->size_.y));
	obj->sprite->setPosition(pos);
	auto physicsBody = PhysicsBody::createBox(Size(obj->sprite->getContentSize()),
		PhysicsMaterial(0.1f, 1.0f, 0.0f));

	//create physics body
	physicsBody->setCategoryBitmask((int)PhysicsCategory::DeathPlane);
	physicsBody->setContactTestBitmask((int)PhysicsCategory::Player);

	//add to scene
	physicsBody->setDynamic(false);
	obj->sprite->setPhysicsBody(physicsBody);
	this->addChild(obj->sprite, 0);
	return obj;
}


void Game::resetGame() {


	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	//reset game values
	coins = 10;
	otherCoins = 10;
	wallTouching = 1;
	jumpTimeout = 0;
	coinsCurrentBlock = 0;
	coinsNextBlock = 0;
	currentDifficulty = START_DIFFICULTY;
	otherDifficulty = START_DIFFICULTY;
	healthToBeAdded = 0;
	coinsCollectedToatl = 0;

	//rebuild first two blocks
	blockNum = 0;
	buildBlock();
	blockNum++;
	buildBlock();

	//reset DDA values
	playerHealth = 100;
	oldHealth = 100;
	hazzardTimeout = HAZZARD_TIMEOUT;
	hazzardsTouching = 0;
	hazzardsTouched = 0;

	//add blank outputs to create gap in text file
	output_->AddOutput(0);
	output_->AddOutput(0);
	output_->AddOutput(0);

	//reset positions
	player->sprite->setPosition(tileWidth*1.75, tileHeight*3.5);
	float labelXPos = (origin.x + visibleSize.width - 16) / 2;
	float y = origin.y + visibleSize.height - 16;
	label->setPosition(Vec2(labelXPos, y));

	//show buttons for method of dda
	gameRunning = false;
	fuzzyButton->setVisible(true);
	crispButton->setVisible(true);
}

Game::~Game() {

	//cleanup pointers
	delete generator;

	for (std::list<GameObject*>::iterator it = blockA.begin(); it != blockA.end(); ++it) {

		(*it)->sprite->removeFromParentAndCleanup(true);
		delete(*it);
	}

	for (std::list<GameObject*>::iterator it = blockB.begin(); it != blockB.end(); ++it) {

		(*it)->sprite->removeFromParentAndCleanup(true);
		delete(*it);
	}

	for (std::list<GameObject*>::iterator it = blockC.begin(); it != blockC.end(); ++it) {

		(*it)->sprite->removeFromParentAndCleanup(true);
		delete(*it);
	}

	player->sprite->removeFromParentAndCleanup(true);
	delete player;
	delete difficulty_;
	delete coins_;
	delete crispDifficulty_;
	delete crispCoins_;
	delete output_;
	fuzzyButton->removeFromParentAndCleanup(true);
	crispButton->removeFromParentAndCleanup(true);
	label->removeFromParentAndCleanup(true);
	labelCoins->removeFromParentAndCleanup(true);
	labelDifficulty->removeFromParentAndCleanup(true);
	labelOtherDifficulty->removeFromParentAndCleanup(true);
}